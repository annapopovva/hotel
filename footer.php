<!-- Footer Area
        ================================================== -->
<footer class="spacer">
    <div class="container">
        <div class="row">

            <?php query_posts('cat=8'); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="col-sm-5">
                <h4><?php the_title(); ?></h4>
                <p><?php echo the_content(); ?></p>
            </div>
            <?php 
                endwhile;
                endif;
            ?>              
             
             <div class="col-sm-3">
                <h4>Бързи връзки</h4>
                <ul class="list-unstyled">
                    <li><a href="rooms-tariff">Стаи и цени</a></li>        
                    <li><a href="introduction">За хотела</a></li>
                    <li><a href="gallery">Галерия</a></li>
                    <li><a href="contact">Контакти</a></li>
                </ul>
            </div>

            <div class="col-sm-4 subscribe">
                <h4>Абонамент за известия</h4>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Въведете своя имейл">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Абонирай ме</button>
                    </span>
                </div>

                <div class="social">
                    <a href="https://www.facebook.com/Uva.Nestum.Wine.SPA/"><i class="fa fa-facebook-square" data-toggle="tooltip" data-placement="top" data-original-title="facebook"></i></a>
                    <a href="#"><i class="fa fa-instagram"  data-toggle="tooltip" data-placement="top" data-original-title="instragram"></i></a>
                    <a href="#"><i class="fa fa-twitter-square" data-toggle="tooltip" data-placement="top" data-original-title="twitter"></i></a>
                    <a href="#"><i class="fa fa-pinterest-square" data-toggle="tooltip" data-placement="top" data-original-title="pinterest"></i></a>
                    <a href="#"><i class="fa fa-tumblr-square" data-toggle="tooltip" data-placement="top" data-original-title="tumblr"></i></a>
                    <a href="https://www.youtube.com/channel/UCXI2riSNBMxAps2nV82FK_A"><i class="fa fa-youtube-square" data-toggle="tooltip" data-placement="top" data-original-title="youtube"></i></a>
                </div>
            </div>
        </div>
        <!--/.row--> 
    </div>
    <!--/.container-->    

<!--/.footer-bottom--> 
</footer>

<div class="text-center copyright">Всички права запазени © 2019 | Изработка и поддръжка от <a href="https://www.facebook.com/annapopovva">Anna Popova</a></div>

<a href="#home" class="toTop scroll"><i class="fa fa-angle-up"></i></a>

<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title">title</h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->    
</div>



<!-- wow script -->
<script src="<?php bloginfo('template_directory'); ?>/assets/wow/wow.min.js"></script>

<!-- uniform -->
<script src="<?php bloginfo('template_directory'); ?>/assets/uniform/js/jquery.uniform.js"></script>


<!-- boostrap -->
<script src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/bootstrap.js" type="text/javascript" ></script>

<!-- jquery mobile -->
<script src="<?php bloginfo('template_directory'); ?>/assets/mobile/touchSwipe.min.js"></script>

<!-- jquery mobile -->
<script src="<?php bloginfo('template_directory'); ?>/assets/respond/respond.js"></script>

<!-- gallery -->
<script src="<?php bloginfo('template_directory'); ?>/assets/gallery/jquery.blueimp-gallery.min.js"></script>


<!-- custom script -->
<script src="<?php bloginfo('template_directory'); ?>/assets/script.js"></script>

</body>
</html>
