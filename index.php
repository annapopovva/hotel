<?php 
/**
 * Template Name: Home page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty_Fourteen 1.0
 */
get_header();
?>

<!-- banner -->
<?php query_posts('cat=6'); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="banner">           
    <?php 
        if ( has_post_thumbnail() ) {
           the_post_thumbnail( 'large', array( 'class' => 'img-responsive' ) );
        } 
    ?>
    <div class="welcome-message">
        <div class="wrap-info">
            <div class="information">
                <h1 class="animated fadeInDown"><?php the_title(); ?></h1>
                <p class="animated fadeInUp"><?php echo the_content(); ?></p>                
            </div>
            <a href="#information" class="arrow-nav scroll wowload fadeInDownBig"><i class="fa fa-angle-down"></i></a>
        </div>
    </div>
</div>

<?php 
    endwhile;
    endif;
?>

<!-- banner-->

<!-- reservation-information -->
<div id="information" class="spacer reserve-info ">
    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-md-8">
                <?php query_posts('cat=7'); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <div class="embed-responsive embed-responsive-16by9 wowload fadeInLeft">
                    <?php echo the_content(); ?>
                </div>

                <?php 
                  endwhile;
                  endif;
                ?>
            </div>

            <div class="col-sm-5 col-md-4">
                <h3>Резервация</h3>
                <form role="form" class="wowload fadeInRight">

                    <div class="form-group">
                        <input type="text" class="form-control"  placeholder="Име">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control"  placeholder="Имейл">
                    </div>
                    <div class="form-group">
                        <input type="Phone" class="form-control"  placeholder="Телефон">
                    </div> 

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <select class="form-control">
                                    <option>Избери апартамент</option>
                                    <option>Апартамент Мезонет</option>
                                    <option>Апартамент Президент</option>
                                    <option>Апартамент Гардън</option>
                                    <option>Двойна стая Делукс</option>
                                    <option>Апартамент Панорама</option>
                                    <option>Двойна стая Класик</option>
                                </select>
                            </div>        
                            <div class="col-xs-6">
                                <select class="form-control">
                                  <option>Брой възрастни</option>
                                  <option>Един</option>
                                  <option>Двама</option>
                                  <option>Трима</option>
                                  <option>Четирима</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <input class="calendar" type="text" id="datepicker" placeholder="Дата на настаняване">
                            </div>
                            <div class="col-xs-6">
                                <input class="calendar" type="text" id="calendar" placeholder="Дата на напускане">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control"  placeholder="Специални изисквания" rows="4"></textarea>
                    </div>

                    <button class="btn btn-default">Резервирай</button>
                </form>    
            </div>
        </div>  
    </div>
</div>

<!-- reservation-information -->

<!-- services -->
<div class="spacer services wowload fadeInUp">
    <div class="container">
        <div class="row">

            <!--slider1-->
            <div class="col-sm-4">
             
                <!-- RoomCarousel -->
                <div id="RoomCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                        <?php query_posts('cat=9'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item active">

                        <?php 
                         if ( has_post_thumbnail() ) {
                           the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                         } 
                        ?>

                        </div>
                        <?php 
                            endwhile;
                            endif;
                        ?>

                        <?php query_posts('cat=10'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item  height-full">
                            <?php 
                            if ( has_post_thumbnail() ) {
                            the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                            } 
                            ?>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#RoomCarousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="right carousel-control" href="#RoomCarousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                </div>
                <!-- RoomCarousel-->
                <div class="caption"><?php the_title(); ?><a href="<?php the_permalink();?>" class="pull-right"><i class="fa fa-edit">Детайли</i></a></div>

                <?php 
                  endwhile;
                  endif;
                ?>
            </div> 
                
                <!--slider2-->
                <div class="col-sm-4">
               
                <!-- RoomCarousel -->
                <div id="RoomCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                        <?php query_posts('cat=13'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item active">

                        <?php 
                         if ( has_post_thumbnail() ) {
                           the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                         } 
                        ?>

                        </div>
                        <?php 
                            endwhile;
                            endif;
                        ?>

                        <?php query_posts('cat=12'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item  height-full">
                            <?php 
                            if ( has_post_thumbnail() ) {
                            the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                            } 
                            ?>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#RoomCarousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="right carousel-control" href="#RoomCarousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                </div>
                <!-- RoomCarousel-->
                <div class="caption"><?php the_title(); ?><a href="<?php the_permalink();?>" class="pull-right"><i class="fa fa-edit">Детайли</i></a></div>

                <?php 
                  endwhile;
                  endif;
                ?>
            </div>

            <!--slider3-->
                <div class="col-sm-4">
               
                <!-- RoomCarousel -->
                <div id="RoomCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                        <?php query_posts('cat=14'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item active">

                        <?php 
                         if ( has_post_thumbnail() ) {
                           the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                         } 
                        ?>

                        </div>
                        <?php 
                            endwhile;
                            endif;
                        ?>

                        <?php query_posts('cat=15'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item  height-full">
                            <?php 
                            if ( has_post_thumbnail() ) {
                            the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                            } 
                            ?>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#RoomCarousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="right carousel-control" href="#RoomCarousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                </div>
                <!-- RoomCarousel-->
                <div class="caption"><?php the_title(); ?><a href="<?php the_permalink();?>" class="pull-right"><i class="fa fa-edit">Детайли</i></a></div>

                <?php 
                  endwhile;
                  endif;
                ?>
            </div>
                   
        </div>
    </div>
</div>
<!-- services -->

<?php 
    get_footer();
?>