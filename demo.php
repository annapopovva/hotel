<?php 
/**
 * Template Name: Demo Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty_Fourteen 1.0
 */
  get_header();
 ?>

<div class="spacer wowload fadeInUp">
    <div class="container">
        <h2>Стаи и Цени</h2>
        <div class="row">

            <!--slider1-->
            <div class="col-sm-6 wowload fadeInUp">
             
                <!-- RoomCarousel -->
                <div id="RoomCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner rooms">

                        <?php query_posts('cat=9'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item active">

                        <?php 
                         if ( has_post_thumbnail() ) {
                           the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                         } 
                        ?>

                        </div>
                        <?php 
                            endwhile;
                            endif;
                        ?>

                        <?php query_posts('cat=10'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="item  height-full">
                            <?php 
                            if ( has_post_thumbnail() ) {
                            the_post_thumbnail('medium', array( 'class' => 'img-responsive'));
                            } 
                            ?>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#RoomCarousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="right carousel-control" href="#RoomCarousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                </div>
                <!-- RoomCarousel-->
                <div class="info">
                    <h3><?php the_title(); ?></h3>
                    <a href="<?php the_permalink();?>" class="btn btn-default">Виж детайли</a>
                </div>

                <?php 
                  endwhile;
                  endif;
                ?>
            </div> 
            </div>
                   
        </div>
    </div>

    <?php 
    get_footer();
?>