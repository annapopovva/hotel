<?php 
/**
 * Template Name: Gallery Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty_Fourteen 1.0
 */
       get_header();
 ?>

<div class="container">

  <h1 class="title">Галерия</h1>
    <div class="row gallery">

      <?php query_posts('cat=5'); ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <div class="col-sm-4 wowload fadeInUp">
        <?php if ( has_post_thumbnail() ) : ?>

        <a href="<?php the_post_thumbnail_url(); ?>" title="<?php the_title_attribute(); ?>" class="gallery-image" data-gallery>
           <?php the_post_thumbnail( 'medium large', array( 'class' => 'img-responsive' ) ); ?>
        </a>

        <?php endif; ?>
      </div>

      <?php 
        endwhile;
        endif;
      ?>

   </div>
</div>

<?php 
    get_footer();
?>