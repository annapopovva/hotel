<?php 
/**
 * Template Name: Introduction Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty_Fourteen 1.0
 */
	get_header();
 ?>

<div class="container">

  <h1 class="title">ДОБРЕ ДОШЛИ В КОМПЛЕКС UVA NESTUM WINE & SPA!</h1>
  <div class="row">

    <?php query_posts('cat=3'); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="col-sm-4">
      <p><?php echo the_content(); ?></p>
    </div>

    <?php 
      endwhile;
      endif;
    ?>

  </div>

  <div class="spacer">

    <?php query_posts('cat=4'); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="embed-responsive embed-responsive-16by9">
      <?php echo the_content(); ?>
    </div>

    <?php 
      endwhile;
      endif;
    ?>
  </div>

</div>

<?php 
    get_footer();
?>