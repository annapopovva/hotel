<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Uva Nestum Wine&SPA</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
  <script>
  $( function() {
    $( "#calendar" ).datepicker();
  } );
  </script>

<!-- Google fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>

<!-- font awesome -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<!-- bootstrap -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/bootstrap/css/bootstrap.min.css" />

<!-- uniform -->
<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/uniform/css/uniform.default.min.css" />

<!-- animate.css -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/wow/animate.css" />

<!-- gallery -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/gallery/blueimp-gallery.min.css">

<!-- favicon -->
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" type="image/x-icon">
<link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" type="image/x-icon">

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/style.css">

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css">

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/jquery-ui-1.12.1.custom/jquery-ui.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/resources/demos/style.css">

</head>

<!--body-->
<body id="home">

<!-- top 
  <form class="navbar-form navbar-left newsletter" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Enter Your Email Id Here">
        </div>
        <button type="submit" class="btn btn-inverse">Subscribe</button>
    </form>
 top -->

<!-- header -->
<nav class="navbar  navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <?php 
        wp_nav_menu(array(
          'theme_location'  => 'top-menu',
          'menu'            => 'navigation',
          'container'       => 'nav',
          'container_id'    => 'main-nav',
          'container_class'    => 'collapse navbar-collapse navbar-right',
          'menu_class'      => 'nav navbar-nav',
          'menu_id'         => 'idm',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
          'depth'           => 0,
          'walker'          => ''
        ));
      ?>    
      </button>
      <a class="navbar-brand" href="index">
        <img class="logo" src="<?php bloginfo('template_directory'); ?>/images/UVAlogo.png"  alt="UVA Nestum">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      <?php 
        wp_nav_menu(array(
          'theme_location'  => 'top-menu',
          'menu'            => 'navigation',
          'container'       => 'nav',
          'container_id'    => 'main-nav',
          'container_class'    => 'collapse navbar-collapse navbar-right',
          'menu_class'      => 'nav navbar-nav',
          'menu_id'         => 'idm',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
          'depth'           => 0,
          'walker'          => ''
        ));
      ?>    
    </div><!-- Wnavbar-collapse -->
  </div><!-- container-fluid -->
</nav>
<!-- header -->

