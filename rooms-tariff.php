<?php 
/**
 * Template Name: Rooms Tariff Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty_Fourteen 1.0
 */
  get_header();
 ?>

<div class="container">

  <h2>Стаи и Цени</h2>

  <!-- form -->

  <div class="row">

    <?php query_posts('cat=11'); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-sm-6 wowload fadeInUp">
      <div class="rooms">

          <?php 
           if ( has_post_thumbnail() ) {
             the_post_thumbnail('full', array( 'class' => 'img-responsive'));
           } 
          ?>

          <div class="info">
            <h3><?php the_title(); ?></h3>
            <a href="<?php the_permalink();?>" class="btn btn-default">Виж детайли</a>
          </div>
        </div>
      </div>

      <?php 
      endwhile;
      endif;
      ?>

  </div>

</div>

<?php 
    get_footer();
?>