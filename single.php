<?php
/**
* Template Name: Room Detail Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
    get_header();
?>

<?php
if ( have_posts() ) {
    while ( have_posts() ) {
        the_post(); 
?>

<div class="container">

  <h1 class="title"><?php the_title(); ?></h1>

 <!-- RoomDetails -->
  <div id="RoomDetails" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        
        <!--item1-->
        
        <div class="item active">

          <?php 
           if ( has_post_thumbnail() ) {
             the_post_thumbnail('full', array( 'class' => 'img-responsive'));
           } 
          ?>

        </div>
       
        
        <!--item2
        <div class="item  height-full">

          <?php 
           if ( has_post_thumbnail() ) {
             the_post_thumbnail('full', array( 'class' => 'img-responsive'));
           } 
          ?>

        </div>-->

      </div>
      <!-- Controls -->
      <a class="left carousel-control" href="#RoomDetails" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
      <a class="right carousel-control" href="#RoomDetails" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
  </div>

  <!-- RoomCarousel-->
  <div class="room-features spacer">
    <div class="row">
      <div class="col-sm-12 col-md-5"> 
        <p><?php echo the_content(); ?></p>
      </div>

      <!--Details-->
      <div class="col-sm-6 col-md-3 amenitites"> 
        <h3>Характеристики на стаята:</h3>    
        <ul>
          <li><strong>Изглед:</strong> Град или Планина</li>
          <li><strong>Декор:</strong>Модерен</li>
          <li><strong>Легла:</strong>Кралско и Двойно</li>
          <li><strong>Доп. легло:</strong>Едно допълнително легло</li>
          <li><strong>Стая за пушачи:</strong>Не</li>
          <li><strong>Баня:</strong>Вана, душ</li>
          <li><strong>Брой възрастни:</strong>Двама и едно дете</li>
        </ul>  
      </div>  

      <div class="col-sm-3 col-md-2">
        <div class="size-price">Размер<span>50кв.м</span></div>
      </div>

      <div class="col-sm-3 col-md-2">
        <div class="size-price">Цена<span>200лв</span></div>
      </div>

    </div>
  </div>
                     
</div> 

<?php } } ?>

<?php
  get_footer();
?>