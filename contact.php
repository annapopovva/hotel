<?php 
/**
 * Template Name: Contact page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty_Fourteen 1.0
 */
	get_header();
 ?>

<div class="container">

<h1 class="title">Контакти</h1>

<!-- form -->
<div class="contact">

   <div class="row">
   	
   		<div class="col-sm-12">
   			<div class="map">
   				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2984.7852116284544!2d23.732168!3d41.573893!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc10218c097f51c00!2sUva+Nestum+Wine+%26+SPA!5e0!3m2!1sen!2sbg!4v1556293681146!5m2!1sen!2sbg" width="100%" height="350" frameborder="0" allowfullscreen></iframe> 	
   			</div>

			<div class="col-sm-6 col-sm-offset-3">
				<div class="spacer">   		
   					<h4>Свържете се с нас</h4>
					<form role="form">
						<div class="form-group">	
							<input type="text" class="form-control" id="name" placeholder="Име">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="email" placeholder="Имейл">
						</div>
						<div class="form-group">
							<input type="phone" class="form-control" id="phone" placeholder="Телефон">
						</div>
						<div class="form-group">
							<textarea type="email" class="form-control"  placeholder="Съобщение" rows="4"></textarea>
						</div>
				
						<button type="submit" class="btn btn-default">Изпрати</button>
					</form>
				</div>
   			</div>

   		</div>
	</div>
</div>
<!-- form -->

</div>

<?php 
    get_footer();
?>